# programming-skills-api

A loopback 4 API linked to a mysql database

## Steps to run the API.

#### 1. Install node modules.
#
> Make sure you has installed node.js and npm in your operative system.

Run in the root project folder:

```sh
npm install
```

#### 2. Create an instance of the mysql database using the docker-compose file.
#
> Make sure you has installed docker-commpose in your operative system.

Run in the root project folder:

```sh
docker-compose up
```

> Note: it's possible that you need use reserved word sudo in linux based systems.

The proccess can take a few minutes wait to see "MySQL init process done. Ready for start up" in the console, and not close the console.

#### 3. Create a database schema from loopback 4 models.
#
> With the previous step you create an empty database, for this reason it's necessary create the tables from loopback 4 models.

Run in the root project folder the following commands:

```sh
npm run build
npm run migrate
```
#### 4. Run API.

Run in the root project folder:
```sh
npm start
```
> When the API is running you can use explore interface to test the CR actions (create and retrieve/read of CRUD). in this url [Explore](http://[::1]:3000/explorer)


