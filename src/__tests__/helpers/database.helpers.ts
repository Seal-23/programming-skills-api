import {PersonRepository, RelationshipRepository} from '../../repositories';
import {testdb} from '../fixtures/datasources/testdb.datasource';

export async function givenEmptyDatabase() {
  let relationshipRepository: RelationshipRepository;
  let personRepository: PersonRepository;
  await new PersonRepository(
    testdb,
    async () => relationshipRepository,
  ).deleteAll();
  await new RelationshipRepository(
    testdb,
    async () => personRepository,
  ).deleteAll();
}
