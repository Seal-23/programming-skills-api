import {expect} from '@loopback/testlab';
import {PersonController, RelationshipController} from '../../../controllers';
import {Person, Relationship} from '../../../models';
import {PersonRepository, RelationshipRepository} from '../../../repositories';
import {testdb} from '../../fixtures/datasources/testdb.datasource';
import {givenEmptyDatabase} from '../../helpers/database.helpers';

describe('Unit test of relationship that has a child', () => {
  it('test has many child reltion', async () => {
    before(givenEmptyDatabase);
    let relationshipRepository: RelationshipRepository;
    const personRepository = new PersonRepository(
      testdb,
      async () => relationshipRepository,
    );
    const relationshipRepo = new RelationshipRepository(
      testdb,
      async () => personRepository,
    );
    const personController = new PersonController(personRepository);
    const relationshipController = new RelationshipController(relationshipRepo);
    const father = await personController.create(
      new Person({
        fullname: 'Adan',
        gender: 'male',
        identificationNumber: 1,
        birth: new Date().toISOString(),
      }),
    );
    const mother = await personController.create(
      new Person({
        fullname: 'Eva',
        gender: 'female',
        identificationNumber: 2,
        birth: new Date().toISOString(),
      }),
    );
    const relation = await relationshipController.create(
      new Relationship({manId: father.id, womanId: mother.id}),
    );
    const child = await personController.create(
      new Person({
        fullname: 'Kain',
        gender: 'male',
        parentsId: relation.id,
        identificationNumber: 3,
        birth: new Date().toISOString(),
      }),
    );
    if (relation.id) {
      const relationship = await relationshipController.findById(relation.id, {
        include: ['children'],
      });
      expect(relationship.children[0]).to.deepEqual(child);
    }
  });
});
