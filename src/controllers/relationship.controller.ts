import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Relationship} from '../models';
import {RelationshipRepository} from '../repositories';

export class RelationshipController {
  constructor(
    @repository(RelationshipRepository)
    public relationshipRepository: RelationshipRepository,
  ) {}

  @post('/relationships')
  @response(200, {
    description: 'Relationship model instance',
    content: {'application/json': {schema: getModelSchemaRef(Relationship)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Relationship, {
            title: 'NewRelationship',
            exclude: ['id'],
          }),
        },
      },
    })
    relationship: Omit<Relationship, 'id'>,
  ): Promise<Relationship> {
    return this.relationshipRepository.create(relationship);
  }

  @get('/relationships/count')
  @response(200, {
    description: 'Relationship model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Relationship) where?: Where<Relationship>,
  ): Promise<Count> {
    return this.relationshipRepository.count(where);
  }

  @get('/relationships')
  @response(200, {
    description: 'Array of Relationship model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Relationship, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Relationship) filter?: Filter<Relationship>,
  ): Promise<Relationship[]> {
    return this.relationshipRepository.find(filter);
  }

  @patch('/relationships')
  @response(200, {
    description: 'Relationship PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Relationship, {partial: true}),
        },
      },
    })
    relationship: Relationship,
    @param.where(Relationship) where?: Where<Relationship>,
  ): Promise<Count> {
    return this.relationshipRepository.updateAll(relationship, where);
  }

  @get('/relationships/{id}')
  @response(200, {
    description: 'Relationship model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Relationship, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Relationship, {exclude: 'where'})
    filter?: FilterExcludingWhere<Relationship>,
  ): Promise<Relationship> {
    return this.relationshipRepository.findById(id, filter);
  }

  @patch('/relationships/{id}')
  @response(204, {
    description: 'Relationship PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Relationship, {partial: true}),
        },
      },
    })
    relationship: Relationship,
  ): Promise<void> {
    await this.relationshipRepository.updateById(id, relationship);
  }

  @put('/relationships/{id}')
  @response(204, {
    description: 'Relationship PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() relationship: Relationship,
  ): Promise<void> {
    await this.relationshipRepository.replaceById(id, relationship);
  }

  @del('/relationships/{id}')
  @response(204, {
    description: 'Relationship DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.relationshipRepository.deleteById(id);
  }
}
