import {Getter, inject} from '@loopback/core';
import {
  BelongsToAccessor,
  DefaultCrudRepository,
  HasManyRepositoryFactory,
  repository,
} from '@loopback/repository';
import {MysqlDataSource} from '../datasources';
import {Person, Relationship, RelationshipRelations} from '../models';
import {PersonRepository} from './person.repository';

export class RelationshipRepository extends DefaultCrudRepository<
  Relationship,
  typeof Relationship.prototype.id,
  RelationshipRelations
> {
  public readonly children: HasManyRepositoryFactory<
    Person,
    typeof Relationship.prototype.id
  >;
  public readonly man: BelongsToAccessor<
    Person,
    typeof Relationship.prototype.id
  >;
  public readonly woman: BelongsToAccessor<
    Person,
    typeof Relationship.prototype.id
  >;

  constructor(
    @inject('datasources.sql') dataSource: MysqlDataSource,
    @repository.getter('PersonRepository')
    personRepositoryGetter: Getter<PersonRepository>,
  ) {
    super(Relationship, dataSource);
    this.children = this.createHasManyRepositoryFactoryFor(
      'children',
      personRepositoryGetter,
    );
    this.man = this.createBelongsToAccessorFor('man', personRepositoryGetter);
    this.woman = this.createBelongsToAccessorFor(
      'woman',
      personRepositoryGetter,
    );
    this.registerInclusionResolver('children', this.children.inclusionResolver);
    this.registerInclusionResolver('woman', this.woman.inclusionResolver);
    this.registerInclusionResolver('man', this.man.inclusionResolver);
  }
}
