import {Getter, inject} from '@loopback/core';
import {
  BelongsToAccessor,
  DefaultCrudRepository,
  repository,
} from '@loopback/repository';
import {MysqlDataSource} from '../datasources';
import {Person, PersonRelations, Relationship} from '../models';
import {RelationshipRepository} from './relationship.repository';

export class PersonRepository extends DefaultCrudRepository<
  Person,
  typeof Person.prototype.id,
  PersonRelations
> {
  public readonly parents: BelongsToAccessor<
    Relationship,
    typeof Person.prototype.id
  >;

  constructor(
    @inject('datasources.sql') dataSource: MysqlDataSource,
    @repository.getter('RelationshipRepository')
    relationshipRepositoryGetter: Getter<RelationshipRepository>,
  ) {
    super(Person, dataSource);
    this.parents = this.createBelongsToAccessorFor(
      'parents',
      relationshipRepositoryGetter,
    );
    this.registerInclusionResolver('parents', this.parents.inclusionResolver);
  }
}
