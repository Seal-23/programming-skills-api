import {belongsTo, Entity, model, property} from '@loopback/repository';
import {Relationship} from './relationship.model';

@model()
export class Person extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      minLength: 2,
      maxLength: 45,
      errorMessage: 'El nombre de usuario debe contener entre 2 y 45',
    },
  })
  fullname: string;

  @property({
    type: 'number',
    required: true,
    index: {
      unique: true,
    },
  })
  identificationNumber: number;

  @property({
    type: 'date',
    required: true,
  })
  birth: string;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      transform: ['toLowerCase'],
      enum: ['male', 'female'],
      errorMessage: 'Debe seleccionar un genero valido',
    },
  })
  gender: 'male' | 'female';

  @belongsTo(() => Relationship)
  parentsId?: number;

  constructor(data?: Partial<Person>) {
    super(data);
  }
}

export interface PersonRelations {
  // describe navigational properties here
}

export type PersonWithRelations = Person & PersonRelations;
