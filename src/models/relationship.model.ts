import {
  belongsTo,
  Entity,
  hasMany,
  model,
  property,
} from '@loopback/repository';
import {Person} from './person.model';

@model()
export class Relationship extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @belongsTo(() => Person)
  manId?: number;

  @belongsTo(() => Person)
  womanId?: number;

  @hasMany(() => Person, {keyTo: 'parentsId'})
  children: Array<Person>;

  constructor(data?: Partial<Relationship>) {
    super(data);
  }
}

export interface RelationshipRelations {}

export type MateWithRelations = Relationship & RelationshipRelations;
